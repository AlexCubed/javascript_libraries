//  ######  ######## ######## ##     ## ######## 
// ##    ## ##          ##    ##     ## ##     ##
// ##       ##          ##    ##     ## ##     ##
//  ######  ######      ##    ##     ## ######## 
//       ## ##          ##    ##     ## ##       
// ##    ## ##          ##    ##     ## ##       
//  ######  ########    ##     #######  ##       

/**
 * 
 * @param {[Number, Number]} position 
 * @param {Canvas} canvas 
 */
function calculate_position(position, canvas) {
	return [position[0] / canvas.width * canvas.inner.width, position[1] / canvas.height * canvas.inner.height];
};

/**
 * 
 * @param {[]} animations 
 * @param {{"draw_line": Number}} options 
 */
function get_animations(animations, options) {
	var animation = "";
	var animation_list = [];
	var extra = "";
	var extra_list = [""];
	if (animations.length > 0) {
		for (var animation of animations) {
			if (animation.name === "builtin_draw_line") {
				animation_list.push(animation.parse(options.draw_line).string);
				extra_list.push(animation.parse(options.draw_line).extra);
			} else {
				animation_list.push(animation.parse().string);
				extra_list.push("transform: scale(0);");
			};
		};
		animation = `animation: ${animation_list.join(", ")};`;
		extra = `${extra_list.join(" ")}`;
	};
	return { animation, extra };
};

//   ###    ##    ## #### ##     ##    ###    ######## ####  #######  ##    ##
//  ## ##   ###   ##  ##  ###   ###   ## ##      ##     ##  ##     ## ###   ##
// ##   ##  ####  ##  ##  #### ####  ##   ##     ##     ##  ##     ## ####  ##
//##     ## ## ## ##  ##  ## ### ## ##     ##    ##     ##  ##     ## ## ## ##
//######### ##  ####  ##  ##     ## #########    ##     ##  ##     ## ##  ####
//##     ## ##   ###  ##  ##     ## ##     ##    ##     ##  ##     ## ##   ###
//##     ## ##    ## #### ##     ## ##     ##    ##    ####  #######  ##    ##

class Animation {
	/**
	 * 
	 * @param {String} name built-in: builtin_draw
	 * @param {Number} [duration] default: 1 
	 * @param {String} [timing_function] default: ease
	 * @param {Number} [delay] default: 0
	 * @param {Number} [iterations] default: 1
	 * @param {"normal"|"reverse"|"alternate"|"alternate_reverse"|"initial"|"inherit"} [direction] default: normal
	 * @param {String} [fill_mode] default: none
	 * @param {String} [play_state] default: running
	 */
	constructor(name, duration, timing_function, delay, iterations, direction, fill_mode, play_state) {
		this.name = name || "";
		this.duration = duration || 1;
		this.timing_function = timing_function || "ease";
		this.delay = delay || 0;
		this.iterations = iterations || 1;
		this.direction = direction || "normal";
		this.fill_mode = fill_mode || "forwards";
		this.play_state = play_state || "running";
	};

	parse(x) {
		var animation = {
			extra: "",
			string: ""
		};
		if (this.name === "builtin_draw_line") {
			animation.extra += `stroke-dasharray: ${x + 2}; stroke-dashoffset: ${x + 3};`;
		};
		animation.string = `${this.name} ${this.duration}s ${this.timing_function} ${this.delay}s ${this.iterations === Infinity ? "infinite" : this.iterations} ${this.direction} ${this.fill_mode} ${this.play_state}`;
		return animation;
	};
};

//  ######     ###    ##    ## ##     ##    ###     ###### 
// ##    ##   ## ##   ###   ## ##     ##   ## ##   ##    ##
// ##        ##   ##  ####  ## ##     ##  ##   ##  ##      
// ##       ##     ## ## ## ## ##     ## ##     ##  ###### 
// ##       ######### ##  ####  ##   ##  #########       ##
// ##    ## ##     ## ##   ###   ## ##   ##     ## ##    ##
//  ######  ##     ## ##    ##    ###    ##     ##  ###### 
//
//     ######   #######  ##    ## ######## ######## ##     ## ########
//    ##    ## ##     ## ###   ##    ##    ##        ##   ##     ##   
//    ##       ##     ## ####  ##    ##    ##         ## ##      ##   
//    ##       ##     ## ## ## ##    ##    ######      ###       ##   
//    ##       ##     ## ##  ####    ##    ##         ## ##      ##   
//    ##    ## ##     ## ##   ###    ##    ##        ##   ##     ##   
//     ######   #######  ##    ##    ##    ######## ##     ##    ##   

class CanvasContext {
	/**
	 * @param {CanvasContext} [c] default: undefined
	 */
	constructor(c) {
		if (c) {
			for (var property in c) {
				this[property] = c[property];
			};
		} else {
			this.animations = [];
			this.background_color = "#000000";
			this.color = "#000000";
			this.fill_color = "#000000";
			this.font = "";
			this.font_size = 32;
			this.origin = [0, 0];
			this.style = "butt";
			this.width = 8;
			this.test = {
				false: true,
				true: false
			};
		};
	};

	/**
	 * Reset the canvas context
	 */
	reset() {
		var new_context = new CanvasContext();
		for (var property in new_context) {
			this[property] = new_context[property];
		};
		return this;
	};
};

//  ######     ###    ##    ## ##     ##    ###     ###### 
// ##    ##   ## ##   ###   ## ##     ##   ## ##   ##    ##
// ##        ##   ##  ####  ## ##     ##  ##   ##  ##      
// ##       ##     ## ## ## ## ##     ## ##     ##  ###### 
// ##       ######### ##  ####  ##   ##  #########       ##
// ##    ## ##     ## ##   ###   ## ##   ##     ## ##    ##
//  ######  ##     ## ##    ##    ###    ##     ##  ###### 

class Canvas {
	/**
	 *
	 * @param {Number} width
	 * @param {Number} height
	 * @param {Number} margin
	 * @param {String} color
	 */
	constructor(width, height, margin, color) {
		this.c = new CanvasContext();
		this.width = (typeof width === "number") ? width : 100;
		this.height = (typeof height === "number") ? height : 100;
		this.margin = (typeof margin === "number") ? margin : 0;
		this.color = (typeof color === "string") ? color : "none";
		this.inner = {
			height: this.height - 2 * this.margin,
			width: this.width - 2 * this.margin
		};
		this.elements = [];
		// this.path = new CanvasPaths();
		this.position = [0, 0];
	};

	/**
	 * Draw an arc at a certain position with a certain radius and an angle range in degrees
	 * @param {[Number, Number]} position
	 * @param {Number} radius
	 * @param {Number} arc_from
	 * @param {Number} arc_to
	 * @param {Boolean} [counter_clockwise] default: false
	 */
	arc(position, radius, arc_from, arc_to, counter_clockwise) {
		// calculate position
		var origin = calculate_position(this.c.origin, this);
		position = calculate_position(position, this);
		// calculate other variables
		var animations = get_animations(this.c.animations, { "draw_line": 32768 });
		arc_to = arc_to === 360 ? 359.999 : arc_to;
		var sweep, large;
		if (counter_clockwise == true) {
			sweep = 0;
			large = 1;
			if (((arc_from - arc_to) % 360 + 360) % 360 <= 180) {
				large = 0;
			} else large = 1;
		} else {
			sweep = 1;
			large = 0;
			if (((arc_from - arc_to) % 360 + 360) % 360 <= 180) {
				large = 1;
			} else large = 0;
		};
		arc_from = arc_from / 180 * Math.PI;
		arc_to = arc_to / 180 * Math.PI;
		// get arc
		var arc = `
			<g
				class="arc_holder"
			>
				<path
					d="
						M${origin[0] + position[0]},${origin[1] + position[1]}
						m${radius * Math.cos(arc_from)},${radius * Math.sin(arc_from)}
						a${radius},${radius},0,${large},${sweep},${radius * Math.cos(arc_to) - radius * Math.cos(arc_from)},${radius * Math.sin(arc_to) - radius * Math.sin(arc_from)}
					"
					pathLength="32768"
					style="
						fill: none;
						stroke: ${this.c.color};
						stroke-linecap: ${this.c.style};
						stroke-linejoin: ${this.c.style};
						stroke-width: ${this.c.width};
						${animations.animation}
						${animations.extra}
						transform-origin: ${origin[0] + position[0]}px ${origin[1] + position[1]}px;
					"
				>
				</path>
			</g>
		`;
		this.elements.push(arc);
		return this;
	};

	/**
	 * Draw a circle at a certain position with a certain radius
	 * @param {[Number, Number]} position
	 * @param {Number} radius
	 */
	circle(position, radius) {
		// calculate position
		var origin = calculate_position(this.c.origin, this);
		position = calculate_position(position, this);
		// calculate other variables
		var animations = get_animations(this.c.animations, { "draw_line": radius * 2 * Math.PI });
		// get circle
		var circle = `
			<g
				class="circle_container"
			>
				<circle
					cx="${origin[0] + position[0]}"
					cy="${origin[1] + position[1]}"
					r="${radius}"
					style="
						fill: ${this.c.fill_color};
						stroke: ${this.c.color};
						stroke-linecap: ${this.c.style};
						stroke-linejoin: ${this.c.style};
						stroke-width: ${this.c.width};
						${animations.animation}
						${animations.extra}
						transform-origin: ${origin[0] + position[0]}px ${origin[1] + position[1]}px;
					"
				>
				</circle>
			</g>
		`;
		this.elements.push(circle);
		return this;
	};

	/**
	 * Draw the canvas and return its html string
	 */
	draw() {
		var elements = ``;
		for (var element of this.elements) {
			elements += element;
		};
		var canvas = `
			<svg
				class="canvas"
				width="${this.width}"
				height="${this.height}"
				x="${this.position[0] || 0}"
				y="${this.position[1] || 0}"
				style="
					/* background-color: ${this.color}; */
					overflow: hidden;
					position: relative;
				"
			>
				<rect
					class="canvas_background"
					x="0"
					y="0"
					width="${this.width}"
					height="${this.height}"
					style="
						fill: ${this.color};
						stroke-width: 0;
					"
				>
				</rect>
				<g
					class="canvas_inner"
					transform="translate(${this.margin}, ${this.margin})"
					style="
						overflow: visible;
					"
				>
					${elements}
				</g>
			</svg>
		`;
		return canvas;
	};

	/**
	 *
	 * @param {[Number, Number]} position
	 * @param {Number[]} values
	 * @param {"bars"|"line"|"points"} type
	 * @param {Number} width
	 * @param {Number} height
	 * @param {Number} margin
	 * @param {Function} [points_function] default: undefined
	 * @param {Number} [max] default: maximum of values
	 */
	graph(position, values, type, width, height, margin, points_function, max) {
		// calculate position
		var origin = calculate_position(this.c.origin, this);
		position = calculate_position(position, this);
		// calculate other variables
		var values = {
			all: values,
			max: (typeof max === "number") ? max : Math.max(...values),
			size: values.length,
			total: values.reduce((a, b) => a + b, 0)
		};
		width = width / this.width * this.inner.width;
		height = height / this.height * this.inner.height;
		position[0] = origin[0] + position[0] - 0.5 * width;
		position[1] = origin[1] + position[1] - 0.5 * height;
		var graph_canvas = new CanvasCanvas(width, height, margin, this.c.background_color, position);
		var scl = graph_canvas.inner.width / graph_canvas.width;
		var scl_inv = 1 / scl;
		graph_canvas.c = new CanvasContext(this.c);
		graph_canvas.c.origin = [0, 0];
		var points = [];
		var settings = {
			center_bar: false,
			extra_margin_top: false
		};
		if (type === "bars") {
			var w = graph_canvas.c.width / 2;
			var r = w / 2;
			for (var i = 0; i < values.size; i++) {
				var x = (settings.center_bar)
					? 0.5 * w * scl_inv + r * scl_inv + (i + 0.5) * (graph_canvas.width - w * scl_inv - 2 * r * scl_inv) / values.size
					: 0.5 * w * scl_inv + r * scl_inv + i * (graph_canvas.width - w * scl_inv - 2 * r * scl_inv) / (values.size - 1);
				var y = (settings.extra_margin_top)
					? graph_canvas.height - (0.5 * w * scl_inv + r * scl_inv + values.all[i] * (graph_canvas.height - w * scl_inv - 2 * r * scl_inv) / (values.max + 1))
					: graph_canvas.height - (0.5 * w * scl_inv + r * scl_inv + values.all[i] * (graph_canvas.height - w * scl_inv - 2 * r * scl_inv) / (values.max));
				graph_canvas.line([x, graph_canvas.height - scl_inv * w], [x, y]);
				points.push({ position: [x, y], value: values.all[i] });
			};
		} else if (type === "line") {
			var vertices = [];
			var w = graph_canvas.c.width / 2;
			var r = w / 2;
			for (var i = 0; i < values.size; i++) {
				var x = (settings.center_bar)
					? 0.5 * w * scl_inv + r * scl_inv + (i + 0.5) * (graph_canvas.width - w * scl_inv - 2 * r * scl_inv) / values.size
					: 0.5 * w * scl_inv + r * scl_inv + i * (graph_canvas.width - w * scl_inv - 2 * r * scl_inv) / (values.size - 1);
				var y = (settings.extra_margin_top)
					? graph_canvas.height - (0.5 * w * scl_inv + r * scl_inv + values.all[i] * (graph_canvas.height - w * scl_inv - 2 * r * scl_inv) / (values.max + 1))
					: graph_canvas.height - (0.5 * w * scl_inv + r * scl_inv + values.all[i] * (graph_canvas.height - w * scl_inv - 2 * r * scl_inv) / (values.max));
				vertices.push([x, y]);
				points.push({ position: [x, y], value: values.all[i] });
			};
			graph_canvas.polyline(vertices);
		} else if (type === "points") {
			var w = graph_canvas.c.width / 2;
			var r = w / 2;
			for (var i = 0; i < values.size; i++) {
				var x = (settings.center_bar)
					? 0.5 * w * scl_inv + r * scl_inv + (i + 0.5) * (graph_canvas.width - w * scl_inv - 2 * r * scl_inv) / values.size
					: 0.5 * w * scl_inv + r * scl_inv + i * (graph_canvas.width - w * scl_inv - 2 * r * scl_inv) / (values.size - 1);
				var y = (settings.extra_margin_top)
					? graph_canvas.height - (0.5 * w * scl_inv + r * scl_inv + values.all[i] * (graph_canvas.height - w * scl_inv - 2 * r * scl_inv) / (values.max + 1))
					: graph_canvas.height - (0.5 * w * scl_inv + r * scl_inv + values.all[i] * (graph_canvas.height - w * scl_inv - 2 * r * scl_inv) / (values.max));
				graph_canvas.c.width /= 2;
				graph_canvas.circle([x, y], r);
				graph_canvas.c.width *= 2;
				points.push({ position: [x, y], value: values.all[i] });
			};
		};
		if (typeof points_function === "function") points_function(graph_canvas, points);
		graph_canvas = graph_canvas.draw();
		// get graph
		var graph = `
			<g
				class="graph_container"
			>
				${graph_canvas}
			</g>
		`;
		this.elements.push(graph);
		return this;
	};

	/**
	 * Draw a line from a certain point to another point
	 * @param {[Number, Number]} from
	 * @param {[Number, Number]} to
	 */
	line(from, to) {
		// calculate positions
		var origin = calculate_position(this.c.origin, this);
		from = calculate_position(from, this);
		to = calculate_position(to, this);
		// calculate other variables
		var length = Math.sqrt(Math.pow(to[0] - from[0], 2) + Math.pow(to[1] - from[1], 2));
		var angle = Math.atan2(to[1] - from[1], to[0] - from[0]);
		var animations = get_animations(this.c.animations, { "draw_line": length });
		// get line
		var line = `
			<g
				class="line_container"
			>
				<line
					x1="${origin[0] + from[0]}"
					x2="${origin[0] + to[0]}"
					y1="${origin[1] + from[1]}"
					y2="${origin[1] + to[1]}"
					style="
						stroke: ${this.c.color};
						stroke-linecap: ${this.c.style};
						stroke-linejoin: ${this.c.style};
						stroke-width: ${this.c.width};
						${animations.animation}
						${animations.extra}
						transform-origin: ${(origin[0] + from[0]) + (0.5 * length * Math.cos(angle))}px ${(origin[1] + from[1]) + (0.5 * length * Math.sin(angle))}px;
					"
				>
				</line>
			</g>
		`;
		this.elements.push(line);
		return this;
	};

	/**
	 * Draw a polygon through the specified vertices
	 * @param {[Number, Number]} vertices
	 */
	polygon(...vertices) {
		if (Array.isArray(vertices) && Array.isArray(vertices[0]) && Array.isArray(vertices[0][0])) vertices = vertices[0];
		// calculate position
		var origin = calculate_position(this.c.origin, this);
		// calculate other variables
		var animations = get_animations(this.c.animations, { "draw_line": 32768 });
		var x = {
			min: undefined,
			max: undefined
		};
		var y = {
			min: undefined,
			max: undefined
		};
		var points = "";
		for (var vertex of vertices) {
			vertex = calculate_position(vertex, this);
			if (x.min === undefined || origin[0] + vertex[0] < x.min) x.min = origin[0] + vertex[0];
			if (x.max === undefined || origin[0] + vertex[0] > x.max) x.max = origin[0] + vertex[0];
			if (y.min === undefined || origin[1] + vertex[1] < y.min) y.min = origin[1] + vertex[1];
			if (y.max === undefined || origin[1] + vertex[1] > y.max) y.max = origin[1] + vertex[1];
			points += ` ${origin[0] + vertex[0]},${origin[1] + vertex[1]}`;
		};
		// get polygon
		var polygon = `
			<g
				class="polygon_container"
			>
				<polygon
					id="test"
					pathLength="32768"
					points="${points}"
					style="
						fill: ${this.c.fill_color};
						stroke: ${this.c.color};
						stroke-linecap: ${this.c.style};
						stroke-linejoin: ${this.c.style};
						stroke-width: ${this.c.width};
						${animations.animation}
						${animations.extra}
						transform-origin: ${(x.max - x.min) / 2 + x.min}px ${(y.max - y.min) / 2 + y.min}px;
					"
				>
				</polygon>
			</g>
		`;
		this.elements.push(polygon);
		return this;
	};

	/**
	 * Draw a line through the specified vertices
	 * @param {[Number, Number]} vertices
	 */
	polyline(...vertices) {
		if (Array.isArray(vertices) && Array.isArray(vertices[0]) && Array.isArray(vertices[0][0])) vertices = vertices[0];
		// calculate position
		var origin = calculate_position(this.c.origin, this);
		// calculate other variables
		var animations = get_animations(this.c.animations, { "draw_line": 32768 });
		var x = {
			min: undefined,
			max: undefined
		};
		var y = {
			min: undefined,
			max: undefined
		};
		var points = "";
		for (var vertex of vertices) {
			vertex = calculate_position(vertex, this);
			if (x.min === undefined || origin[0] + vertex[0] < x.min) x.min = origin[0] + vertex[0];
			if (x.max === undefined || origin[0] + vertex[0] > x.max) x.max = origin[0] + vertex[0];
			if (y.min === undefined || origin[1] + vertex[1] < y.min) y.min = origin[1] + vertex[1];
			if (y.max === undefined || origin[1] + vertex[1] > y.max) y.max = origin[1] + vertex[1];
			points += ` ${origin[0] + vertex[0]},${origin[1] + vertex[1]}`;
		};
		// get polyline
		var polyline = `
			<g
				class="polyline_container"
			>
				<polyline
					pathLength="32768"
					points="${points}"
					style="
						fill: none;
						stroke: ${this.c.color};
						stroke-linecap: ${this.c.style};
						stroke-linejoin: ${this.c.style};
						stroke-width: ${this.c.width};
						${animations.animation}
						${animations.extra}
						transform-origin: ${(x.max - x.min) / 2 + x.min}px ${(y.max - y.min) / 2 + y.min}px;
					"
				>
				</polyline>
			</g>
		`;
		this.elements.push(polyline);
		return this;
	};

	/**
	 * Draw a rectangle from a certain point to another point
	 * @param {[Number, Number]} from
	 * @param {[Number, Number]} to
	 */
	rect(from, to) {
		// calculate position
		var origin = calculate_position(this.c.origin, this);
		from = calculate_position(from, this);
		to = calculate_position(to, this);
		// calculate other variables
		var width = Math.abs(to[0] - from[0]);
		var height = Math.abs(to[1] - from[1]);
		var animations = get_animations(this.c.animations, { "draw_line": 2 * width + 2 * height });
		// get rect
		var rect = `
			<g
				class="rect_container"
			>
				<rect
					height="${height}"
					x="${origin[0] + from[0]}"
					y="${origin[1] + from[1]}"
					width="${width}"
					style="
						fill: ${this.c.fill_color};
						stroke: ${this.c.color};
						stroke-linecap: ${this.c.style};
						stroke-linejoin: ${this.c.style};
						stroke-width: ${this.c.width};
						${animations.animation}
						${animations.extra}
						transform-origin: ${origin[0] + from[0] + 0.5 * width}px ${origin[1] + from[1] + 0.5 * height}px;
					"
				>
				</rect>
			</g>
		`;
		this.elements.push(rect);
		return this;
	};

	/**
	 * Draw text at a certain position
	 * @param {[Number, Number]} position
	 * @param {String} text
	 * @param {"end"|"middle"|"start"} [anchor] default: "middle"
	 */
	text(position, text, anchor) {
		var text_value = text;
		// calculate position
		var origin = calculate_position(this.c.origin, this);
		position = calculate_position(position, this);
		// calculate other variables
		var animations = get_animations(this.c.animations, { "draw_line": 32768 });
		if (anchor !== "end" && anchor !== "middle" && anchor !== "start") anchor = "middle";
		// get text
		var text = `
			<g
				class="text_container"
			>
				<text
					font-size="${this.c.font_size}"
					x="${origin[0] + position[0]}"
					y="${origin[1] + position[1]}"
					style="
						cursor: default;
						dominant-baseline: central;
						fill: ${this.c.fill_color};
						font-family: ${this.c.font};
						stroke: ${this.c.color};
						stroke-linecap: ${this.c.style};
						stroke-linejoin: ${this.c.style};
						stroke-width: ${this.c.width};
						text-anchor: ${anchor};
						${animations.animation}
						${animations.extra}
						transform-origin: ${origin[0] + position[0]}px ${origin[1] + position[1]}px;
					"
				>
					${text_value}
				</text>
			</g>
		`;
		this.elements.push(text);
		return this;
	};
};

//  ######     ###    ##    ## ##     ##    ###     ###### 
// ##    ##   ## ##   ###   ## ##     ##   ## ##   ##    ##
// ##        ##   ##  ####  ## ##     ##  ##   ##  ##      
// ##       ##     ## ## ## ## ##     ## ##     ##  ###### 
// ##       ######### ##  ####  ##   ##  #########       ##
// ##    ## ##     ## ##   ###   ## ##   ##     ## ##    ##
//  ######  ##     ## ##    ##    ###    ##     ##  ###### 
//
//     ######     ###    ##    ## ##     ##    ###     ###### 
//    ##    ##   ## ##   ###   ## ##     ##   ## ##   ##    ##
//    ##        ##   ##  ####  ## ##     ##  ##   ##  ##      
//    ##       ##     ## ## ## ## ##     ## ##     ##  ###### 
//    ##       ######### ##  ####  ##   ##  #########       ##
//    ##    ## ##     ## ##   ###   ## ##   ##     ## ##    ##
//     ######  ##     ## ##    ##    ###    ##     ##  ###### 

class CanvasCanvas extends Canvas {
	constructor(width, height, margin, color, position) {
		super(width, height, margin, color);
		this.position = [
			(typeof position !== "undefined" && typeof position[0] === "number") ? position[0] : 0,
			(typeof position !== "undefined" && typeof position[1] === "number") ? position[1] : 0
		];
	};
};